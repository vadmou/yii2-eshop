<?php

use yii\db\Migration;

class m161121_184827_category extends Migration
{
    public function up()
    {
        $this->createTable('category', [
            'id'          => $this->primaryKey(),
            'parent_id'   => $this->integer(10)->notNull()->defaultValue(0),
            'name'        => $this->string()->notNull(),
            'keywords'    => $this->string(),
            'description' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('category');
    }

}
