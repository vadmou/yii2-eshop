<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use app\models\Product;
use yii\data\Pagination;


class CategoryController extends AppController
{

    public function actionIndex()
    {
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
//        echo '<pre>';
//        print_r($hits);
//        echo '</pre>';
        $this->setMeta('Eshop');
        return $this->render('index', compact('hits'));
    }

    public function actionView($id)
    {
//        $id = Yii::$app->request->get('id');

        $category = Category::findOne($id);
        if (empty($category)) {
            throw new \yii\web\HttpException(404, 'Такой категории нет.');
        }

//        $products=Product::find()->where(['category_id'=>$id])->all();

        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3, 'forcePageParam' => false,
            'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();


        $this->setMeta('Eshop | ' . $category->name, $category->keywords, $category->description);
        return $this->render('view', compact('products', 'pages', 'category'));
    }

    public function actionSearch()
    {
        $search = trim(Yii::$app->request->get('search'));
        $this->setMeta('Eshop | Поиск: ' . $search);

        if (!$search) {
            return $this->render('search');
        }

        $query = Product::find()->where(['like', 'name', $search]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 3, 'forcePageParam' => false,
            'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('search', compact('products', 'pages', 'search'));
    }
}